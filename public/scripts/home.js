$(function(){
    
    function getScope(ctrlName) {
        var sel = 'body[ng-controller="' + ctrlName + '"]';
        return angular.element(sel).scope();
    }
    
    function buildEventsOnMap(mylat, mylng) {
    $.get('events/lat/' + mylat + '/lng/' + mylng, function(response) {
        
        if (response.status == 'SUCCESS') {
            for(i in response.data) {
                var event = response.data[i];
                event.url = 'events/' + event.id;
                if (typeof markers[event.id] === 'object') {
                    continue;
                }
                markers[event.id] = new google.maps.Marker({
                    position: {lat: parseFloat(event.lat), lng: parseFloat(event.lng)},
                    map: map,
                    title: 'Click to reserve an event ' + event.name,
                    event_id: event.id,
                    expo_event: event
                });
                infoWindows[event.id] = new google.maps.InfoWindow({
                    content: '<div>Event: ' + event.name + '<br/>'
                            + 'Event location: ' + event.hall_name + '<br>'
                            + 'Event starts: ' + event.start_date + '<br>'
                            + 'Event ends: ' + event.end_date + '<br>'
                            + '</div>'
                });
                
                markers[event.id].addListener('click', function(){
                    closeAllInfoWindows();
                    var that = this;
                    infoWindows[this.event_id].open(map, this);
                    var controllerScope = getScope('homeCtrl');
                    controllerScope.$apply(function(){
                        controllerScope.eventInfoShow = true;
                        controllerScope.event = that.expo_event;
                    })
                });
                
                infoWindows[event.id].addListener('closeclick', function() {
                    var controllerScope = getScope('homeCtrl');
                    controllerScope.$apply(function(){
                        controllerScope.eventInfoShow = false;
                        controllerScope.event = null;
                        
                    })
                });
            }
        }
    });
    }
    
    buildEventsOnMap(lat, lng);
    
    google.maps.event.addListener(map, 'center_changed',
        function(){
            var mapCenter = map.getCenter();
            buildEventsOnMap(mapCenter.lat(), mapCenter.lng());
        });
    
    
    function closeAllInfoWindows(){
        if (!infoWindows.length) {
            return false;
        }
        
        for(i in infoWindows) {
            infoWindows[i].close();
        }
    }
    
    // Sets the map on all markers in the array.
    function setMapOnAll(map) {
        for (var i = 0; i < markers.length; i++) {
          markers[i].setMap(map);
        }
    }
    
    // Removes the markers from the map, but keeps them in the array.
    function clearMarkers() {
        setMapOnAll(null);
    }
    
});

angular.module('homeApp', [])
        .controller('homeCtrl', ['$scope', function($scope) {
                $scope.eventInfoShow = false;
                $scope.event = null;
                
                function showEventInfo() {
                    $scope.eventInfoShow = true;
                }
                
                function isEventInfoShown() {
                    return $scope.eventInfoShow;
                }
                
                $scope.showEventInfo = showEventInfo;
                $scope.isEventInfoShown = isEventInfoShown;
        }]);