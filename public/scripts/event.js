$(function() {});

angular.module('eventApp', [])
        .directive("ngMobileClick", [function () {
            return function (scope, elem, attrs) {
                elem.bind("touchstart click", function (e) {
                    scope.$apply(attrs["ngMobileClick"]);
                });
            }
        }])
        .controller('eventCtrl', ['$scope', '$http', function ($scope, $http) {
                $scope.loader = false;
                $scope.stands = null;
                $scope.initMyApp = function (value) {
                    $scope.event_id = value
                    $scope.loader = true;
                    $http({
                        method: 'GET',
                        url: '/stands/event/' + $scope.event_id
                    }).then(function successCallback(response) {
                        if (response.data.status == 'SUCCESS') {
                            $scope.stands = response.data.data;
                        }
                        $scope.loader = false;
                    }, function errorCallnack(response) {
                        $scope.stands = null;
                        $scope.loader = false;
                    });
                }

                $scope.selectedStand = null;

                function openStandDetails(stand) {
                    if (stand.booked) {
                        return true;
                    }
                    console.log(stand);
                    $scope.selectedStand = stand;
                    $('.modal').modal('show');
                }
                
                $scope.openStandDetails = openStandDetails;

            }]);