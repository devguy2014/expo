'use strict';

$(function() {});


angular.module('bookApp', ['angularFileUpload'])


    .controller('bookCtrl', ['$scope', '$http', 'FileUploader', function($scope, $http, FileUploader) {
            
        $scope.myfiles = [];
        
        $scope.files_selected = false;
        $scope.document_selected = false;
        $scope.logo_selected = false;
        
        
        var uploader = $scope.uploader = new FileUploader({
            url: '/company/document'
        });

        // FILTERS

        uploader.filters.push({
            name: 'customFilter',
            fn: function(item /*{File|FileLikeObject}*/, options) {
                return this.queue.length < 10;
            }
        });

        // CALLBACKS

        uploader.onAfterAddingFile = function(fileItem) {
            
            angular.forEach(angular.element('input[type="file"]'), function(value, key){
                var a = angular.element(value);
                if (a.prop('value').search(fileItem.file.name) >= 0) {
                    var index = -1;
                    if ($scope.myfiles.length) {
                        angular.forEach($scope.myfiles, function(v, k) {
                            if (a.prop('id') === v.field) {
                                index = k;
                            }
                        })
                    }
                    if (index == -1)  {
                        index = $scope.myfiles.length;
                    }
                    if (a.prop('id') == 'document') {
                        $scope.document_selected = true;
                    }
                    if (a.prop('id') == 'logo') {
                        $scope.logo_selected = true;
                    }
                    $scope.myfiles[index] = {name: fileItem.file.name, field: a.prop('id')}
                    return false;
                }
            });
            //console.info($scope.myfiles)
        };
        uploader.onAfterAddingAll = function(addedFileItems) {
            if ($scope.myfiles.length > 1) {
                $scope.files_selected = true;
            }
            //console.info($scope.myfiles.length)
        };
        uploader.onBeforeUploadItem = function(item) {
            $scope.loader = true;
            var field = '';
            angular.forEach($scope.myfiles, function(value, key) {
                if (value.name == item.file.name) {
                    field = value.field;
                    return false;
                }
            })
            item.formData.push({
                _token: $scope.csrf_token,
                field: field,
                form_id: $scope.form_id,
                stand_id: $scope.stand_id,
                event_id: $scope.event_id
            });
        };
        uploader.onProgressItem = function(fileItem, progress) {
            
        };
        uploader.onProgressAll = function(progress) {
            
        };
        uploader.onSuccessItem = function(fileItem, response, status, headers) {
            
        };
        uploader.onErrorItem = function(fileItem, response, status, headers) {
            fileItem.isUploaded = false;
        };
        uploader.onCancelItem = function(fileItem, response, status, headers) {
        };
        uploader.onCompleteItem = function(fileItem, response, status, headers) {
            
        };
        uploader.onCompleteAll = function() {
            $scope.loader = false;
            
            $http({
                method: 'POST',
                url: 'stands/' + $scope.stand_id + '/event/' + $scope.event_id + '/book',
                data: {
                    firstname: $scope.firstname,
                    lastname: $scope.lastname,
                    email: $scope.email,
                    company: $scope.company
                }
            }).then(function success(response){
                $scope.reservation_result = response.data.message;
                if (response.data.status == "SUCCESS") {
                    $scope.reserved = true;
                } else {
                    $scope.reserved = false;
                }
                $('.modal').modal('show');
            }, function error(){
                $scope.reservation_result = 'Occurred error';
                $scope.reserved = false;
                $('.modal').modal('show');
            })
            
        };


        
        $scope.uploader = uploader;
        
        $scope.submit = function() {
            
        }
    }]);