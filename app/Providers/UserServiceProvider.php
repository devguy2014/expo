<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

/**
 * User Service provider
 *
 * @author Rustamjon Mukhammadaliyev <rustaonline@yahoo.com>
 */
class UserServiceProvider extends ServiceProvider
{
    /**
     * Register company service.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton(\App\Http\Controllers\Controller::SERVICE_USER, function() {
            return new \App\Services\UserService();
        });
    }
}
