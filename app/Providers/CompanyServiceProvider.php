<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

/**
 * Description of CompanyServiceProvider
 *
 * @author Rustamjon Mukhammadaliyev <rustaonline@yahoo.com>
 */
class CompanyServiceProvider extends ServiceProvider
{
    /**
     * Register company service.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton(\App\Http\Controllers\Controller::SERVICE_COMPANY, function() {
            return new \App\Services\CompanyService();
        });
    }
}
