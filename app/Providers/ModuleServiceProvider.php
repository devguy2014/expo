<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

/**
 * ModuleServiceProvider class adds modules views into Laravel
 *
 * @author Rustamjon Mukhammadaliyev <rustaonline@yahoo.com>
 */
class ModuleServiceProvider extends ServiceProvider
{
    public function boot() {
        $modules = config("modules.modules");
        foreach ($modules as $module) {
            if(is_dir(dirname(__DIR__) . '/Modules/' . $module.'/Views')) {
                $this->loadViewsFrom(dirname(__DIR__) . '/Modules/' . $module . '/Views', $module);
            }
        }
    }
    
    public function register()
    {
        
    }
}
