<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class CommonServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton(\App\Http\Controllers\Controller::SERVICE_COMMON, function() {
            return new \App\Services\CommonService();
        });
    }
}
