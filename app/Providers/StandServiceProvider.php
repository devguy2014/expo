<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

/**
 * ServiceProvider for Stand
 *
 * @author Rustamjon Mukhammadaliyev <rustaonline@yahoo.com>
 */
class StandServiceProvider extends ServiceProvider
{
    public function register() {
        $this->app->singleton(\App\Http\Controllers\Controller::SERVICE_STAND, function() {
            return new \App\Services\StandService();
        });
    }
}
