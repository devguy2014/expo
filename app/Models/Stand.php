<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Description of Stand
 *
 * @author Rustamjon Mukhammadaliyev <rustaonline@yahoo.com>
 */
class Stand extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'styles', 'image', 'price', 'hall_id'
    ];
    
    public function booked()
    {
        return $this->hasOne('App\BookedStands', 'stand_id');
    }
}
