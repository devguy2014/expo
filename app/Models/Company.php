<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Description of BookedStands
 *
 * @author Rustamjon Mukhammadaliyev <rustaonline@yahoo.com>
 */
class Company extends Model
{
    protected $table = 'companies';
    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'document', 'logo'
    ];
}
