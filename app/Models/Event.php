<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Description of Events
 *
 * @author Rustamjon Mukhammadaliyev <rustaonline@yahoo.com>
 */
class Event extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'hall_id', 'start_date', 'end_date'
    ];
    
    /**
     * Get the start date.
     *
     * @param  string  $value
     * @return string
     */
    public function getStartDateAttribute($value)
    {
        $dt = new \DateTime($value);
        return $dt->format('F j, Y');
    }
    
    /**
     * Get the end date.
     *
     * @param  string  $value
     * @return string
     */
    public function getEndDateAttribute($value)
    {
        $dt = new \DateTime($value);
        return $dt->format('F j, Y');
    }
}
