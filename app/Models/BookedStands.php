<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Description of BookedStands
 *
 * @author Rustamjon Mukhammadaliyev <rustaonline@yahoo.com>
 */
class BookedStands extends Model
{
    protected $table = 'booked_stands';
    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'stand_id', 'event_id', 'company_id'
    ];
}
