<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Description of Halls
 *
 * @author Rustamjon Mukhammadaliyev <rustaonline@yahoo.com>
 */
class Hall extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'lat', 'lng',
    ];
}
