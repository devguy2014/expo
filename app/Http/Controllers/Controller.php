<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesResources;

class Controller extends BaseController
{
    const RESPONSE_SUCCESS = 'SUCCESS';
    const RESPONSE_FAILURE = 'FAILURE';
    
    const COMPANY_LOGO = 'logo';
    const COMPANY_DOCUMENT = 'document';
    
    const SERVICE_COMMON = 'common';
    const SERVICE_COMPANY = 'company';
    const SERVICE_STAND = 'stand';
    const SERVICE_USER = 'user';
    
    use AuthorizesRequests, AuthorizesResources, DispatchesJobs, ValidatesRequests;
    
    /**
     * 
     * @param string $status
     * @param string $message
     * @param mixed $data
     * @return array
     */
    protected function respond($status, $message, $data = null) {
        if (empty($data)) {
            return ['status' => $status, 'message' => $message];
        }
        
        return ['status' => $status, 'message' => $message, 'data' => $data];
    }
}
