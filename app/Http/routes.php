<?php

/*
  |--------------------------------------------------------------------------
  | Application Routes
  |--------------------------------------------------------------------------
  |
  | Here is where you can register all of the routes for an application.
  | It's a breeze. Simply tell Laravel the URIs it should respond to
  | and give it the controller to call when that URI is requested.
  |
 */


// here we load modules routes
$modules = config("modules.modules");
foreach ($modules as $module) {
    if (file_exists(dirname(__DIR__) . '/Modules/' . $module . '/routes.php')) {
        include dirname(__DIR__) . '/Modules/' . $module . '/routes.php';
    }
}
