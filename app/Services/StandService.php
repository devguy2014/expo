<?php

namespace App\Services;

use DB;
use Log;
use App\Models\Event;
use App\Models\BookedStands;

/**
 * Stand related service methods
 *
 * @author Rustamjon Mukhammadaliyev <rustaonline@yahoo.com>
 */
class StandService
{
    
    /**
     * Returns stands of events
     * 
     * @param int $event_id Event ID
     * @return array
     */
    public function getByEventId($event_id)
    {
        $event = Event::find($event_id);
        if (empty($event)) {
            Log::notice('Event not found.');
            return [];
        }

        $stands = DB::table('stands')
                ->select('stands.*', 'booked_stands.id AS booked',
                        'companies.document','companies.logo',
                        'users.firstname','users.lastname')
                ->leftJoin('booked_stands', 'stands.id', '=', 'booked_stands.stand_id')
                ->leftJoin('companies', 'booked_stands.company_id', '=', 'companies.id')
                ->leftJoin('users', 'companies.id', '=', 'users.company_id')
                ->where('stands.hall_id', '=', $event->hall_id)
                ->get();
        
        if (!count($stands)) {
            Log::notice('Stands not found');
            return [];
        }

        foreach($stands as $key => $stand) {
            if (!empty($stand->booked)) {
                $stands[$key]->title = 'Booked';
                $stands[$key]->show_price = false;
            } else {
                $stands[$key]->title = 'Free';
                $stands[$key]->show_price = true;
            }
        }
        
        return $stands;
    }
    
    /**
     * Books stand for company
     * 
     * @param int $stand_id
     * @param int $event_id
     * @param int $company_id
     * @return int
     */
    public function bookStand($stand_id, $event_id, $company_id)
    {
        $booked_stand = new BookedStands([
            'stand_id' => $stand_id,
            'event_id' => $event_id,
            'company_id' => $company_id]
            );
        
        $booked_stand->save();
        
        return $booked_stand->id;;
    }
}
