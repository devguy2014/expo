<?php

namespace App\Services;

use App\Models\User;

/**
 * Users related related service methods
 *
 * @author Rustamjon Mukhammadaliyev <rustaonline@yahoo.com>
 */
class UserService
{
    /**
     * Adds user
     * 
     * @param string $firstname
     * @param string $lastname
     * @param string $email
     * @param int $company_id
     * @return int
     */
    public function addUser($firstname, $lastname, $email, $company_id)
    {
        $user = new User([
                'firstname' => $firstname,
                'lastname' => $lastname,
                'email' => $email,
                'company_id' => $company_id,
            ]);
            
        $user->save();
        
        return $user->id;
    }
}
