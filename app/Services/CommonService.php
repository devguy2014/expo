<?php

namespace App\Services;

use Session;
use DB;

/**
 * Description of CommonService
 *DB;
 * @author Rustamjon Mukhammadaliyev <rustaonline@yahoo.com>
 */
class CommonService
{
    /**
     * Checks wheather logo and document file names in session.
     * 
     * @param string $logo
     * @param string $document
     * @throws \Exception
     */
    public function validateUploads($logo, $document)
    {
        if (empty($logo) || empty($document)) {
            $message = 'Company document or logo is not uploaded.';
            if (Session::has('file_error')) {
                $message .= ' '. Session::get('file_error');
            }
            throw new \Exception($message);
        }
    }
    
    /**
     * Cleans session of logo and document
     */
    public function cleanUploadSession()
    {
        Session::forget(\App\Http\Controllers\Controller::COMPANY_LOGO);
        Session::forget(\App\Http\Controllers\Controller::COMPANY_DOCUMENT);
    }
    
    /**
     * Returns finished events
     * 
     * @return mixed 
     */
    public function getFinishedEvents()
    {
        $days_difference = env('DAYS_DIFFERENCE', 1);
        
        $result = DB::table('events')
                ->join('booked_stands', 'events.id', '=', 'booked_stands.event_id')
                ->join('users', 'users.company_id', '=', 'booked_stands.company_id')
                ->join('stands', 'stands.id', '=', 'booked_stands.stand_id')
                ->join('companies', 'companies.id', '=', 'booked_stands.company_id')
                ->leftJoin('mails', 'events.id', '=', 'mails.event_id')
                ->where('events.end_date', '=', DB::raw('subdate(current_date, ' . $days_difference . ')'))
                ->havingRaw('mid IS NULL')
                ->select('events.*', 'users.firstname', 'users.lastname', 'users.email',
                        'companies.name as company', 'mails.id as mid', 'stands.name as stand',
                        DB::raw('DATE_FORMAT(events.start_date,\'%d %b %Y\') AS start'),
                        DB::raw('DATE_FORMAT(events.end_date,\'%d %b %Y\') AS end'))
                ->get();
        
        if (!count($result)) {
            return false;
        }
        
        return $result;
    }
    
    /**
     * Returns the nearest events
     * 
     * @param float $lat
     * @param float $lng
     * @return mixed
     */
    public function getNearestEvents($lat, $lng)
    {
        $radius = env('MAP_SEARCH_RADIUS', 3);
        
        $q = "SELECT e.*,h.name as hall_name,h.lat,h.lng, "
                . "(6371 * acos(sin(radians($lat)) * sin(radians(`lat`)) + cos(radians($lat)) * cos(radians(`lat`)) * cos((radians(`lng`) - radians($lng))))) AS `distance` "
                . "FROM `halls` h JOIN `events` e ON h.id=e.hall_id HAVING `distance` <= $radius ORDER BY `distance`";
        
        $events = DB::select($q);
        
        if (!count($events)) {
            return false;
        }
        
        return $events;
    }
    
    /**
     * 
     * @param int $event_id
     */
    public function eventMailed($event_id)
    {
        DB::table('mails')->insert(['event_id' => $event_id]);
    }
}
