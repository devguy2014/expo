<?php

namespace App\Services;

use App\Models\Company;

/**
 * Company related related service methods
 *
 * @author Rustamjon Mukhammadaliyev <rustaonline@yahoo.com>
 */
class CompanyService
{
    /**
     * Adds company
     * 
     * @param string $name Company name
     * @param string $logo Logo file path
     * @param string $document Document file path
     * @return int Company ID
     */
    public function addCompany($name, $logo, $document)
    {
        $company = new Company(['name' => $name,
            'logo' => $logo,
            'document' => $document
            ]);
        
        $company->save();
        
        return $company->id;
    }
}
