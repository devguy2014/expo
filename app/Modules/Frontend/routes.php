<?php

/**
 * Routes for Frontend module
 * 
 * @author Rustamjon Mukhammadaliyev <rustaonline@yahoo.com>
 */

// Home page
Route::get('/', function() {
    return view('Frontend::home', ['page' => 'home', 'title' => 'ExpoBooking']);
});

// Events page
Route::get('/events/{event_id}', '\App\Modules\Frontend\Controllers\PageController@event');

Route::get('/stands/{stand_id}/event/{event_id}/book', '\App\Modules\Frontend\Controllers\PageController@booking');