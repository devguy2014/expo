<?php

namespace App\Modules\Frontend\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Event;
use App\Models\Hall;
use App\Models\BookedStands;
use App\Models\Stand;

/**
 * Page controller
 *
 * @author Rustamjon Mukhammadaliyev <rustaonline@yahoo.com>
 */
class PageController extends Controller
{
    /**
     * Events page
     * 
     * @param int $event Event ID
     * @return Illuminate\View\View
     */
    public function event($event_id)
    {
        $event = Event::find($event_id);
        
        $hall = Hall::find($event->hall_id);

        return view('Frontend::event', ['event' => $event, 'hall' => $hall, 'title' => 'Stands selector']);
    }
    
    /**
     * Registration page
     * 
     * @param int $stand_id
     * @param int $event_id
     * @return Illuminate\View\View
     */
    public function booking($stand_id, $event_id)
    {
        $form_id = uniqid();
        $event = Event::find($event_id);
        if (is_object($event)) {
            $hall = Hall::find($event->hall_id);
        }
        $stand = Stand::find($stand_id);
        
        $booked = BookedStands::where('stand_id', $stand_id)->where('event_id', $event_id)->first();
        if (!empty($booked)) {
            return redirect('/events/' . $event_id);
        }
        
        return view('Frontend::book', [
            'event' => $event,
            'hall' => $hall, 
            'stand' => $stand,
            'form_id' => $form_id, 
            'title' => 'Reserve stand']);
    }
}
