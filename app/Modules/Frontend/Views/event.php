<!DOCTYPE html>
<html ng-app="eventApp">
    <head>
        <base href="/">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta charset="UTF-8">
        <title><?= $title?></title>
        
        <link rel="stylesheet" href="bower_components/bootstrap/dist/css/bootstrap.min.css">
        <link rel="stylesheet" href="bower_components/bootstrap/dist/css/bootstrap-theme.min.css">
        <link rel="stylesheet" href="styles/styles.css">
    </head>
    <body ng-controller="eventCtrl" ng-init="initMyApp(<?=$event->id?>);hall='<?= $hall->name?>'">
        <div class="expo-container">
            <?php include(__DIR__ . '/chunks/menu.php') ?>
            
            <div class="panel panel-primary">
                <div class="panel-body">
                    <h2><?= $title?></h2>
                    
                    <p class="lead">Event: <?=$event->name?><br/>
                    Expo center: <?= $hall->name?><br/>
                    Date: <?=$event->start_date?> - <?=$event->end_date?><br/>
                    
                    
                    </p>
                    
                    <div class="hall-stands">
                        <img class="img-thumbnail" src="uploads/halls/<?php echo $hall->image ?>">
            
            
            
                        <div ng-repeat="stand in stands" class="stand" ng-class="{ 'clickable' : !stand.booked, 'booked' : stand.booked }" style="{{stand.styles}}" href="#{{stand.id}}" 
                             alt="{{stand.name}}" title="{{stand.name}}"
                             data-stand="{{stand.id}}" ng-mobile-click="openStandDetails(stand)">
                            <div class="stand-price" ng-hide="stand.booked">${{stand.price}}</div>
                            <div class="company-logo" ng-show="stand.booked"><img height="20" ng-src="{{stand.logo}}" /></div>
                            <div class="stand-title">{{stand.title}}</div>
                            <div class="company-document" ng-show="stand.booked"><a target="_blank" ng-href="{{stand.document}}"><span class="glyphicon glyphicon-cloud-download"></span></a></div>
                            <div class="company-contact" ng-show="stand.booked">{{stand.firstname}} {{stand.lastname}}</div>
                        </div>
                    </div>
                </div>
            </div>
</div>

        <div class="tail">&nbsp;</div>
            
        
        
        <div class="modal fade" tabindex="-1" role="dialog">
            <div class="modal-dialog" role="document">
              <div class="modal-content">
                <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                  <h4 class="modal-title">{{hall}} - {{selectedStand.name}} <small>${{selectedStand.price}}</small></h4>
                </div>
                <div class="modal-body">
                    <div>
                        <img class="stand-image img-rounded" ng-src="uploads/stands/{{selectedStand.image}}"/>
                    </div>
                </div>
                <div class="modal-footer">
                  <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                  <a class="btn btn-info" ng-href="stands/{{selectedStand.id}}/event/<?=$event->id?>/book">Reserve</a>
                </div>
              </div><!-- /.modal-content -->
            </div><!-- /.modal-dialog -->
        </div><!-- /.modal -->
        
        <div class="loader" ng-show="loader"><img class="img-thumbnail" src="images/loader.gif"/></div>
        <script src="bower_components/jquery/dist/jquery.min.js"></script>
        <script src="bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
        <script src="bower_components/angularjs/angular.min.js"></script>
        <script src="scripts/event.js"></script>
    </body>
</html>
