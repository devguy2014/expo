<!DOCTYPE html>
<html ng-app="homeApp">
    <head>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta charset="UTF-8">
        <title><?= $title?></title>

        <link rel="stylesheet" href="bower_components/bootstrap/dist/css/bootstrap.min.css">
        <link rel="stylesheet" href="bower_components/bootstrap/dist/css/bootstrap-theme.min.css">
        <link rel="stylesheet" href="styles/styles.css">
    </head>
    <body ng-controller="homeCtrl">
        <div class="container">
            <div class="col-sm-1 col-md-3"></div>
            <div class="col-xs-12 col-sm-10 col-md-6">
                <?php require_once(__DIR__ . '/chunks/menu.php') ?>
                <div class="panel panel-primary">
                    <div class="panel-body">
                        <h2><?= $title?></h2>
                        <div id="map"></div>
                        <div class="home-panel panel panel-info" ng-show="isEventInfoShown()">
                            <div class="panel-heading">
                                <h3 class="panel-title">Event info</h3>
                            </div>
                            <div class="panel-body">
                                <p class="lead">
                                    Event: {{event.name}}<br/>
                                    Expo center: {{event.hall_name}}<br/>
                                    Date: {{event.start_date}} - {{event.end_date}}
                                </p>

                                <a ng-href="{{event.url}}" class="btn btn-info">Book your place</a>
                            </div>
                        </div>


                    </div>
                </div>
            </div>
            <div class="col-sm-1 col-md-3"></div>
        </div>
        <script>
                    var map;
                    var markers = [];
                    var lat = 41.299;
                    var lng = 69.2401;
                    var infoWindows = [];
                            function initMap() {
                                var mapDiv = document.getElementById('map');
                                map = new google.maps.Map(mapDiv, {
                                    center: {lat: lat, lng: lng},
                                    zoom: 13
                                });
                            }
        </script>
        <script src="https://maps.googleapis.com/maps/api/js?key=<?= env('GOOGLE_API_KEY')?>&callback=initMap"></script>
        <script src="bower_components/jquery/dist/jquery.min.js"></script>
        <script src="bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
        <script src="bower_components/angularjs/angular.min.js"></script>
        <script src="scripts/home.js"></script>
    </body>
</html>
