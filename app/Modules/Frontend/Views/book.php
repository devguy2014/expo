<!DOCTYPE html>
<html ng-app="bookApp">
    <head>
        <base href="/">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta charset="UTF-8">
        <title><?= $title?></title>

        <link rel="stylesheet" href="bower_components/bootstrap/dist/css/bootstrap.min.css">
        <link rel="stylesheet" href="bower_components/bootstrap/dist/css/bootstrap-theme.min.css">
        <link rel="stylesheet" href="styles/styles.css">
    </head>
    <body ng-controller="bookCtrl" ng-init="loader=false">
        
        <div class="container">

            <div class="col-sm-1 col-md-3"></div>
            
            <div class="col-xs-12 col-sm-10 col-md-6">
                
                <?php require_once(__DIR__ . '/chunks/menu.php') ?>
                
                
                
                <div class="panel panel-primary">
                    <div class="panel-body">
                        
                        <h2><?= $title?></h2>
                        
                        <?php if (empty($event) || empty($stand)) { ?>
                        
                        <div class="alert alert-warning no-margin-bottom" role="alert"><strong> Oops!</strong> Event or Stand information is not found. Please, go to <a href="/">home</a> page and try to reserve stand.</div>
                        
                        <?php } else { ?>
                        
                        <div class="book-hall-stands">
                            <img class="img-thumbnail" src="uploads/stands/<?=$stand->image?>"/>
                        </div>
                        
                        <p class="lead">Event: <?= $event->name?><br/>
                    Expo center: <?= $hall->name?><br/>
                    Date: <?= $event->start_date?> - <?= $event->end_date?><br/>
                    Price: $<?= $stand->price?></p>
                        
                        <form name="myForm">
                            <input type="hidden" name="_token" value="<?php echo csrf_token() ?>" ng-init="csrf_token='<?php echo csrf_token() ?>'">
                            <input type="hidden" name="stand_id" value="<?= $stand->id ?>" ng-init="stand_id=<?= $stand->id ?>">
                            <input type="hidden" name="event_id" value="<?= $event->id ?>" ng-init="event_id=<?= $event->id ?>">
                            <input type="hidden" name="form_id" value="<?= $form_id ?>" ng-init="form_id='<?= $form_id ?>'">
                            <div class="form-group" ng-class="{ 'has-error' : myForm.$dirty && myForm.firstname.$error.required }">
                                <label for="firstname">Firstname</label>
                                <input type="text" class="form-control" id="firstname" name="firstname" placeholder="Firstname" ng-model="firstname" required />
                                <span class="help-block" ng-show="myForm.$dirty && myForm.firstname.$error.required">Firstname is required</span>
                            </div>
                            <div class="form-group" ng-class="{ 'has-error' : myForm.$dirty && myForm.lastname.$error.required }">
                                <label for="lastname">Lastname</label>
                                <input type="text" class="form-control" id="lastname" name="lastname" placeholder="Lastname" ng-model="lastname" required />
                                <span class="help-block" ng-show="myForm.$dirty && myForm.lastname.$error.required">Lastname is required</span>
                            </div>
                            <div class="form-group" ng-class="{ 'has-error' : myForm.$dirty && (myForm.email.$error.required || myForm.email.$invalid) }">
                                <label for="email">Email address</label>
                                <input type="email" required class="form-control" id="email" name="email" placeholder="Email" ng-model="email" />
                                <span class="help-block" ng-show="myForm.$dirty && myForm.email.$error.required">Email address is required</span>
                                <span class="help-block" ng-show="myForm.$dirty && myForm.email.$invalid">Email address is invalid </span>
                            </div>
                            
                            <div class="form-group" ng-class="{ 'has-error' : myForm.$dirty && myForm.company.$error.required }">
                                <label for="company">Company</label>
                                <input type="text" required class="form-control" id="company" name="company" placeholder="Company" ng-model="company" />
                                <span class="help-block" ng-show="myForm.company.$error.required">Company is required</span>
                            </div>
                            
                            <div class="form-group">
                                <label for="document">Marketing document</label>
                                <input type="file" required id="<?= \App\Http\Controllers\Controller::COMPANY_DOCUMENT?>" name="<?= \App\Http\Controllers\Controller::COMPANY_DOCUMENT?>" nv-file-select uploader="uploader">
                                <p class="help-block"  ng-hide="document_selected">Document is required</span>
                            </div>
                            
                            <div class="form-group">
                                <label for="logo">Company logo</label>
                                <input type="file" required id="<?= \App\Http\Controllers\Controller::COMPANY_LOGO?>" name="<?= \App\Http\Controllers\Controller::COMPANY_LOGO?>" nv-file-select uploader="uploader">
                                <p class="help-block" ng-hide="logo_selected">Logo is required</span>
                            </div>
                            <a href="events/<?= $event->id?>" class="btn btn-default">Back</a>
                            <button type="button" class="btn btn-info" ng-click="uploader.uploadAll()" ng-disabled="myForm.$invalid || !files_selected">Confirm Reservation</button>
                            
                        </form>
                        
                        <?php } ?>

                    </div>
                </div>
                
            </div>
            
            <div class="col-sm-1 col-md-3"></div>
            
        </div>
        
        <div class="modal fade" tabindex="-1" role="dialog">
            <div class="modal-dialog" role="document">
              <div class="modal-content">
                <div class="modal-header">
                  <h4 class="modal-title">Reservation status</h4>
                </div>
                <div class="modal-body">
                    {{reservation_result}}
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal" ng-show="!reserved">Close</button>
                    <a class="btn btn-info" href="events/<?=$event->id?>" ng-show="reserved">Ok</a>
                </div>
              </div><!-- /.modal-content -->
            </div><!-- /.modal-dialog -->
        </div><!-- /.modal -->
        
        
        <div class="loader" ng-show="loader"><img class="img-thumbnail" src="images/loader.gif"/></div>
        <script src="bower_components/jquery/dist/jquery.min.js"></script>
        <script src="bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
        <script src="bower_components/angularjs/angular.min.js"></script>
        <script src="bower_components/angular-file-upload/dist/angular-file-upload.min.js"></script>
        <script src="scripts/book.js"></script>
    </body>
</html>
