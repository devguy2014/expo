<?php

/**
 * Routes for WebService module
 * 
 * @author Rustamjon Mukhammadaliyev <rustaonline@yahoo.com>
 */


Route::get('/send', '\App\Modules\WebService\Controllers\ReportController@send');
Route::get('/events', '\App\Modules\WebService\Controllers\EventController@all');
Route::get('/events/lat/{lat}/lng/{lng}', '\App\Modules\WebService\Controllers\EventController@nearest');
Route::get('/stands/event/{event_id}', '\App\Modules\WebService\Controllers\StandController@getStands');

Route::post('/stands/{stand_id}/event/{event_id}/book', '\App\Modules\WebService\Controllers\StandController@book');
Route::post('/company/document', '\App\Modules\WebService\Controllers\StandController@companyDocument');