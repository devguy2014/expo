<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
        <style>
#customers {
    font-family: "Trebuchet MS", Arial, Helvetica, sans-serif;
    border-collapse: collapse;
    width: 100%;
}

#customers td, #customers th {
    border: 1px solid #ddd;
    padding: 8px;
}

#customers tr:nth-child(even){background-color: #f2f2f2;}

#customers tr:hover {background-color: #ddd;}

#customers th {
    padding-top: 12px;
    padding-bottom: 12px;
    text-align: left;
    background-color: #4CAF50;
    color: white;
}
</style>
    </head>
    <body>
        <h2>Information about finished events, booked stands and users</h2>
        <table id="customers">
            <tr>

                <th>ID</th>
                <th>Event</th>
                <th>Stand</th>
                <th>Start</th>
                <th>End</th>
                <th>Customer</th>
                <th>Customer email</th>
                <th>Company</th>
            </tr>
        <?php foreach($data as $row) { ?>
            <tr>
                <td><?= $row->id?></td>
                <td><?= $row->name?></td>
                <td><?= $row->stand?></td>
                <td><?= $row->start?></td>
                <td><?= $row->end?></td>
                <td><?= $row->firstname . ' ' . $row->lastname?></td>
                <td><?= $row->email?></td>
                <td><?= $row->company?></td>
            </tr>
        <?php } ?>
        </table>
    </body>
</html>
