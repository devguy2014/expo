<?php

namespace App\Modules\WebService\Controllers;

use App\Http\Controllers\Controller;

/**
 * Event web service controller class
 *
 * @author Rustamjon Mukhammadaliyev <rustaonline@yahoo.com>
 * @package WebService\Controller\Event
 */
class EventController extends Controller
{
    /**
     * Lookup for events in N km radius. N is equal to MAP_SEARCH_RADIUS which is set in .env file
     * 
     * @param float $lat Latitude
     * @param float $lng Longitude
     * @return array
     */
    public function nearest($lat, $lng)
    {
        $events = app(static::SERVICE_COMMON)->getNearestEvents($lat, $lng);

        if ($events === false) {
            return $this->respond(static::RESPONSE_FAILURE, 'Events are not found');
        }
        foreach($events as $key => $value) {
            $start_dt = new \DateTime($value->start_date);
            $events[$key]->start_date = $start_dt->format('F j, Y');
            $end_dt = new \DateTime($value->end_date);
            $events[$key]->end_date = $end_dt->format('F j, Y');
        }
        return $this->respond(static::RESPONSE_SUCCESS, 'Events are found', $events);
    }
}
