<?php

namespace App\Modules\WebService\Controllers;

use App\Http\Controllers\Controller;
use Log;
use Illuminate\Http\Request;
use App\Models\Event;
use App\Models\Stand;
use App\Models\BookedStands;
use Session;
use DB;

/**
 * Stand webservice controller class
 *
 * @author Rustamjon Mukhammadaliyev <rustaonline@yahoo.com>
 */
class StandController extends Controller
{
    
    /**
     * Returns list of stands for event
     * 
     * @param int $event_id Event id
     * @return array
     */
    public function getStands($event_id)
    {
        $stands = app(static::SERVICE_STAND)->getByEventId($event_id);
        if (count($stands)) {
            return $this->respond(static::RESPONSE_SUCCESS, 'Stands are found.', $stands);
        }
        
        return $this->respond(static::RESPONSE_FAILURE, 'Event not found.');
    }

    /**
     * Reserves stand for user
     * 
     * @param Request $request
     * @param int $stand_id
     * @param int $event_id
     * @return array
     */
    public function book(Request $request, $stand_id, $event_id)
    {
        $common_service = app(static::SERVICE_COMMON);
        
        $logo = Session::get(static::COMPANY_LOGO);
        $document = Session::get(static::COMPANY_DOCUMENT);
        
        try {
            $common_service->validateUploads($logo, $document);
        } catch (\Exception $e) {
            Log::warning($e->getMessage());
            return $this->respond(static::RESPONSE_FAILURE, $e->getMessage());
        }
        
        $stand = Stand::find($stand_id);
        $event = Event::find($event_id);
        
        if (empty($stand) || empty($event)) {
            Log::warning('Invalid request');
            return $this->respond(static::RESPONSE_FAILURE, 'Invalid request');
        }
        
        DB::beginTransaction();
        
        try {
            $company_id = app(static::SERVICE_COMPANY)
                    ->addCompany($request->input('company'), $logo, $document);
            
            app(static::SERVICE_USER)->addUser(
                    $request->input('firstname'),
                    $request->input('lastname'),
                    $request->input('email'),
                    $company_id);
            
            app(static::SERVICE_STAND)->bookStand($stand->id, $event->id, $company_id);
            
            DB::commit();
        } catch (\Exception $e) {
            DB::rollBack();
            Log::error($e->getMessage());
            return $this->respond(static::RESPONSE_FAILURE, 'Reserving of stand is failed.');
        }
        
        $common_service->cleanUploadSession();
        
        return $this->respond(static::RESPONSE_SUCCESS, 'Stand is booked.');
    }
    
    /**
     * Uploads company document and logo here
     * 
     * @param Request $request
     * @return array
     */
    public function companyDocument(Request $request)
    {
        $field = static::COMPANY_DOCUMENT;
        if ($request->input('field') == static::COMPANY_LOGO) {
            $field = static::COMPANY_LOGO;
        }
        
        $form_id = $request->input('form_id');
        if (empty($form_id)) {
            Log::warning('Invalid form id');
            return $this->respond(static::RESPONSE_FAILURE, 'Form id is invalid.');
        }
        
        $stand_id = $request->input('stand_id');
        $event_id = $request->input('event_id');
        
        $booked = BookedStands::where('stand_id', $stand_id)->where('event_id', $event_id)->first();
        
        if (is_object($booked)) {
            $message = 'Stand has reserved already.';
            Log::notice($message);
            return $this->respond(static::RESPONSE_FAILURE, $message, ['reserved' => 1]);
        }
        
        $www_path = 'uploads/' . $field . '/' . $form_id;
        $folder = public_path($www_path);
        if (!is_dir($folder)) {
            set_error_handler(function () { return true; });
            $result = mkdir($folder, 0755, true);
            restore_error_handler();
            if (!$result) {
                Log::error("Unable to create folder $folder");
                return $this->respond(static::RESPONSE_FAILURE, 'Upload folder is not created.');
            }
        }
        if (!is_writable($folder)) {
            Log::error("Folder $folder is not writable");
            return $this->respond(static::RESPONSE_FAILURE, 'Upload folder is not writable.');
        }
        
        $file = $request->file('file');
        if (!($file instanceof \Illuminate\Http\UploadedFile)) {
            Log::error("Uploaded file is invalid.");
            return $this->respond(static::RESPONSE_FAILURE, 'File is invalid.');
        }
        
        try {
            $ext = $file->getClientOriginalExtension();
            $file->move($folder, $field . '.' . $ext);
            $document = $www_path . '/' . $field . '.' . $ext;
            $status = static::RESPONSE_SUCCESS;
            $message = 'File is uploaded.';
            Session::put($field, $document);
            //Log::info($field . '-' . $document . ' : ' . Session::get($field));
        } catch (\Exception $e) {
            $message = $e->getMessage();
            Log::error($message);
            Session::put('file_error', $message);
            $status = static::RESPONSE_FAILURE;
        }
        return $this->respond($status, $message, ['v' => Session::get($field)]);
    }
    
}
