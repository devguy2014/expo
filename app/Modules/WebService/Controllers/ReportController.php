<?php

namespace App\Modules\WebService\Controllers;

use App\Http\Controllers\Controller;
use DB;
use Log;
use Mail;

/**
 * Report webservice controller class
 *
 * @author Rustamjon Mukhammadaliyev <rustaonline@yahoo.com>
 */
class ReportController extends Controller
{
    /**
     * 
     * @return array
     */
    public function send()
    {
        $days_difference = env('DAYS_DIFFERENCE', 1);
        
        $result = app(static::SERVICE_COMMON)->getFinishedEvents();
        
        if ($result === false) {
            Log::info("No events to notifiy admin.");
            return $this->respond(static::RESPONSE_SUCCESS, 'No mail sent');
        }
        
        Log::info("Mail sending is started");
        $mail = Mail::send('WebService::email.stats', ['data' => $result], function ($message) {

            $message->from(env('MAIL_ADMIN'), env('MAIL_ADMIN_NAME'));

            $message->to(env('MAIL_ADMIN'))->subject('Information about finished events');

        });
        
        if ($mail) {
            $ids = [];
            foreach($result as $row) {
                $ids[$row->id] = $row->id;
            }
            foreach($ids as $id) {
                app(static::SERVICE_COMMON)->eventMailed($id);
            }
        }
        Log::info("Mail sending is stopped");
        return $this->respond(static::RESPONSE_SUCCESS, 'Notifying is finished');
    }
}
