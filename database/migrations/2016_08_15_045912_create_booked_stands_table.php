<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBookedStandsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('booked_stands', function (Blueprint $table) {
            $table->increments('id')->unsigned();
            $table->integer('stand_id')->unsigned();
            $table->integer('event_id')->unsigned();
            $table->integer('company_id')->unsigned();
            $table->timestamps();
            $table->index(['stand_id', 'event_id', 'company_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('booked_stands');
    }
}
