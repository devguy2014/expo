<?php

use Illuminate\Database\Seeder;
use App\Models\Hall;
use App\Models\Event;
use App\Models\Stand;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        
        $halls = [
            ['lat' => 41.305, 'lng' => 69.248, 'image' => 'hall1.png',  'name' => 'ExpoCentral'],
            ['lat' => 41.293, 'lng' => 69.224, 'image' => 'hall2.png',  'name' => 'Tashkent Hall'],
            ['lat' => 41.321, 'lng' => 69.239, 'image' => 'hall3.png',  'name' => 'World Expo'],
            ['lat' => 41.311, 'lng' => 69.252, 'image' => 'hall4.png',  'name' => 'ICT Expo'],
            ['lat' => 41.326, 'lng' => 69.235, 'image' => 'hall5.png',  'name' => 'Expo Co.'],
            ['lat' => 41.344, 'lng' => 69.206, 'image' => 'hall6.png',  'name' => 'Asian'],
            ['lat' => 41.337, 'lng' => 69.255, 'image' => 'hall7.png',  'name' => 'North Expo'],
            ['lat' => 41.338, 'lng' => 69.271, 'image' => 'hall8.png',  'name' => 'Guest House Expo'],
            ['lat' => 41.317, 'lng' => 69.280, 'image' => 'hall9.png',  'name' => 'Sample Expo'],
            ['lat' => 41.301, 'lng' => 69.283, 'image' => 'hall10.png', 'name' => 'ABC Hall']
        ];
        
        $events = [
            ['name' => 'Textile Expo',      'start_date' => '2016-09-22', 'end_date' => '2016-09-24', 'hall_id' => 0],
            ['name' => 'Food Expo',         'start_date' => '2016-09-05', 'end_date' => '2016-09-08', 'hall_id' => 0],
            ['name' => 'Auto Expo',         'start_date' => '2016-10-10', 'end_date' => '2016-10-13', 'hall_id' => 0],
            ['name' => 'ICT Expo',          'start_date' => '2016-11-24', 'end_date' => '2016-11-25', 'hall_id' => 0],
            ['name' => 'Ads Expo',          'start_date' => '2016-11-14', 'end_date' => '2016-11-16', 'hall_id' => 0],
            ['name' => 'Furniture Expo',    'start_date' => '2016-12-16', 'end_date' => '2016-12-18', 'hall_id' => 0],
            ['name' => 'Wood Expo',         'start_date' => '2016-12-28', 'end_date' => '2016-12-30', 'hall_id' => 0],
            ['name' => 'Air Show',          'start_date' => '2016-11-03', 'end_date' => '2016-11-05', 'hall_id' => 0],
            ['name' => 'Sport Expo',        'start_date' => '2016-10-10', 'end_date' => '2016-10-12', 'hall_id' => 0],
            ['name' => 'HighTech Expo',     'start_date' => '2016-09-15', 'end_date' => '2016-09-17', 'hall_id' => 0],
        ];
        
        $stands = [
            [
                ['hall_id' => 0, 'styles' => 'top: 51px;left: 61px;height: 95px;width: 77px;', 'price' => 0, 'name' => 'Stand', 'image' => 'stand1.jpg'],
                ['hall_id' => 0, 'styles' => 'top: 51px;left: 178px;height: 95px;width: 77px;', 'price' => 0, 'name' => 'Stand', 'image' => 'stand2.jpg'],
                ['hall_id' => 0, 'styles' => 'top: 51px;left: 294px;height: 95px;width: 77px;', 'price' => 0, 'name' => 'Stand', 'image' => 'stand3.jpg'],
                ['hall_id' => 0, 'styles' => 'top: 51px;left: 411px;height: 95px;width: 77px;', 'price' => 0, 'name' => 'Stand', 'image' => 'stand4.jpg'],
                ['hall_id' => 0, 'styles' => 'top: 51px;left: 527px;height: 95px;width: 77px;', 'price' => 0, 'name' => 'Stand', 'image' => 'stand5.jpg'],
                ['hall_id' => 0, 'styles' => 'top: 315px;left: 61px;height: 95px;width: 77px;', 'price' => 0, 'name' => 'Stand', 'image' => 'stand6.jpg'],
                ['hall_id' => 0, 'styles' => 'top: 315px;left: 178px;height: 95px;width: 77px;', 'price' => 0, 'name' => 'Stand', 'image' => 'stand7.jpg'],
                ['hall_id' => 0, 'styles' => 'top: 315px;left: 294px;height: 95px;width: 77px;', 'price' => 0, 'name' => 'Stand', 'image' => 'stand8.jpg'],
                ['hall_id' => 0, 'styles' => 'top: 315px;left: 411px;height: 95px;width: 77px;', 'price' => 0, 'name' => 'Stand', 'image' => 'stand9.jpg'],
                ['hall_id' => 0, 'styles' => 'top: 315px;left: 527px;height: 95px;width: 77px;', 'price' => 0, 'name' => 'Stand', 'image' => 'stand10.jpg'],
            ],
            [
                ['hall_id' => 0, 'styles' => 'width:77px;height:95px;top:51px;left:61px', 'price' => 0, 'name' => 'Stand', 'image' => 'stand11.jpg'],
                ['hall_id' => 0, 'styles' => 'width:77px;height:95px;top:51px;left:294px', 'price' => 0, 'name' => 'Stand', 'image' => 'stand12.jpg'],
                ['hall_id' => 0, 'styles' => 'width:77px;height:95px;top:51px;left:527px', 'price' => 0, 'name' => 'Stand', 'image' => 'stand13.jpg'],
                ['hall_id' => 0, 'styles' => 'width:77px;height:95px;top:183px;left:178px', 'price' => 0, 'name' => 'Stand', 'image' => 'stand14.jpg'],
                ['hall_id' => 0, 'styles' => 'width:77px;height:95px;top:183px;left:411px', 'price' => 0, 'name' => 'Stand', 'image' => 'stand15.jpg'],
                ['hall_id' => 0, 'styles' => 'width:77px;height:95px;top:315px;left:61px', 'price' => 0, 'name' => 'Stand', 'image' => 'stand16.jpg'],
                ['hall_id' => 0, 'styles' => 'width:77px;height:95px;top:315px;left:294px', 'price' => 0, 'name' => 'Stand', 'image' => 'stand17.jpg'],
                ['hall_id' => 0, 'styles' => 'width:77px;height:95px;top:315px;left:527px', 'price' => 0, 'name' => 'Stand', 'image' => 'stand18.jpg'],
            ]
        ];

        for($i = 0; $i < 10; $i++) {
            $hall = new Hall($halls[$i]);
            $hall->save();
            $events[$i]['hall_id'] = $hall->id;
            
            if (fmod($i, 2) == 0) {
                $stand_rows = $stands[0];
            } else {
                $stand_rows = $stands[1];
            }
            
            $k = 1;
            foreach($stand_rows as $stand_row) {
                $price = rand(1500, 9900);
                $stand_row['hall_id'] = $hall->id;
                $stand_row['price'] = $price/100;
                $stand_row['name'] .= " $k";
                $stand = new Stand($stand_row);
                $stand->save();
                $k++;
            }
            
            $event = new Event($events[$i]);
            $event->save();
        }
    }
}
