<!DOCTYPE html>
<html>
    <head>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta charset="UTF-8">
        <title>Page not found</title>
        <base href="<?php echo url('/');?>"/>

        <link rel="stylesheet" href="bower_components/bootstrap/dist/css/bootstrap.min.css">
        <link rel="stylesheet" href="bower_components/bootstrap/dist/css/bootstrap-theme.min.css">
        <link rel="stylesheet" href="styles/styles.css">
    </head>
    <body>
        <div class="container">
            <div class="col-sm-1 col-md-3"></div>
            <div class="col-xs-12 col-sm-10 col-md-6">
                <div class="jumbotron">
                    <h2>Oops, something goes wrong</h2>
                    <p><a class="btn btn-primary btn-lg" href="<?php echo url('/');?>" role="button">Go home</a></p>
                </div>
            </div>
            <div class="col-sm-1 col-md-3"></div>
        </div>
    </body>
</html>
