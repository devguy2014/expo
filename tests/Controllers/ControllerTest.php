<?php

use App\Http\Controllers\Controller;

/**
 * Description of ControllerTest
 *
 * @author Rustamjon Mukhammadaliyev <rustaonline@yahoo.com>
 */
class ControllerTest extends PHPUnit_Framework_TestCase
{
    
    /**
     * @covers \App\Http\Controllers\Controller::respond
     */
    public function testRespondWithData()
    {
        $controller = new Controller();
        
        $data = [ 'test' => 'Stand name' ];
        
        $result = $this->invokeMethod($controller, 'respond', [Controller::RESPONSE_SUCCESS, 'API message', $data]);
        
        $this->assertEquals('SUCCESS', $result['status']);
        $this->assertEquals('API message', $result['message']);
        $this->assertEquals('Stand name', $result['data']['test']);
    }
    
    /**
     * @covers \App\Http\Controllers\Controller::respond
     */
    public function testRespondWithoutData()
    {
        $controller = new Controller();
        
        $result = $this->invokeMethod($controller, 'respond', [Controller::RESPONSE_FAILURE, 'Error message']);
        
        $this->assertEquals('FAILURE', $result['status']);
        $this->assertEquals('Error message', $result['message']);
    }
    
    /**
     * Call protected/private method of a class.
     *
     * @param object &$object    Instantiated object that we will run method on.
     * @param string $methodName Method name to call
     * @param array  $parameters Array of parameters to pass into method.
     *
     * @return mixed Method return.
     */
    protected function invokeMethod(&$object, $methodName, array $parameters = array())
    {
        $reflection = new \ReflectionClass(get_class($object));
        $method = $reflection->getMethod($methodName);
        $method->setAccessible(true);

        return $method->invokeArgs($object, $parameters);
    }
    
}
