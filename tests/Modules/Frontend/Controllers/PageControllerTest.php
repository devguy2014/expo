<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use AspectMock\Test as test;


class PageControllerTest extends TestCase
{
    
    /**
     * Checking Book your place button on homepage.
     *
     * @return void
     */
    public function testHome()
    {
        $this->visit('/')
             ->see('Book your place');
    }
    
    /**
     * Checking Home link on stands exposition page
     * @covers \App\Modules\Frontend\Controllers\PageController::event
     * @covers \App\Models\Event::getStartDateAttribute
     * @covers \App\Models\Event::getEndDateAttribute
     * @return void
     */
    public function testEventPage()
    {
        $this->visit('/events/1')
             ->click('Home')
             ->seePageIs('/');
    }
    
    /**
     * Checking Confirm Reservation link on registration page
     * 
     * @covers \App\Modules\Frontend\Controllers\PageController::booking
     * @return void
     */
    public function testBookingPage()
    {
        $this->visit('/stands/2/event/1/book')
             ->see('Confirm Reservation');
    }
    
    /**
     * @covers \App\Exceptions\Handler::render
     * @covers \App\Exceptions\Handler::report
     */
    public function test404Page()
    {
        $this->get('/nosuchpage')
            ->assertResponseStatus(404)
            ->see('Oops, page not found');
    }
    
    /**
     * @covers \App\Exceptions\Handler::render
     * @covers \App\Exceptions\Handler::report
     */
    public function testErrorPage()
    {
        $this->get('/events/987654321')
            ->assertResponseStatus(503)
            ->see('Oops, something goes wrong');
    }
}
