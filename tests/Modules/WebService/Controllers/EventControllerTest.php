<?php

/**
 * Description of EventControllerTest
 *
 * @author Rustamjon Mukhammadaliyev <rustaonline@yahoo.com>
 */
class EventControllerTest extends TestCase
{
    /**
     * @covers \App\Modules\WebService\Controllers\EventController::nearest
     * @covers \App\Services\CommonService::getNearestEvents
     */
    public function testNearestSuccess()
    {
        $this->json('GET', '/events/lat/41.299/lng/69.2401')
             ->seeJson([
                 "status" => "SUCCESS",
                 "message" => "Events are found",
             ]);
    }
    
    /**
     * @covers \App\Modules\WebService\Controllers\EventController::nearest
     * @covers \App\Services\CommonService::getNearestEvents
     */
    public function testNearestFailure()
    {
        $this->json('GET', '/events/lat/4.299/lng/6.2401')
             ->seeJson([
                 "status" => "FAILURE",
                 "message" => "Events are not found",
             ]);
    }
}
