<?php

/**
 * Description of StandControllerTest
 *
 * @author Rustamjon Mukhammadaliyev <rustaonline@yahoo.com>
 */
class StandControllerTest extends TestCase
{
    /**
     * @covers \App\Modules\WebService\Controllers\StandController::getStands
     * @covers \App\Services\StandService::getByEventId
     */
    public function testGetStandsFailure()
    {
        $this->json('GET', '/stands/event/562')
             ->seeJson([
                 "status" => "FAILURE",
                 "message" => "Event not found."
             ]);
    }
    
    /**
     * @covers \App\Modules\WebService\Controllers\StandController::getStands
     * @covers \App\Services\StandService::getByEventId
     */
    public function testGetStandsNoStandsFailure()
    {
        $event = new \App\Models\Event([
            'name' => 'My Event 1',
            'start_date' => \DB::raw('now()'),
            'end_date' => \DB::raw('now()'),
            'hall_id' => 1000
        ]);
        
        $event->save();
        $this->json('GET', '/stands/event/' . $event->id)
             ->seeJson([
                 "status" => "FAILURE",
                 "message" => "Event not found."
             ]);
        
        $event->delete();
    }
    
    /**
     * @covers \App\Modules\WebService\Controllers\StandController::getStands
     * @covers \App\Services\StandService::getByEventId
     */
    public function testGetStandsSuccess()
    {
        $params = [
            'company' => 'Company',
            'firstname' => 'Rustamjon',
            'lastname' => 'Mukhammadaliyev',
            'email' => 'rustaonline@yahoo.com',
        ];
        
        $this->withSession(['document' => 'file1.pdf', 'logo' => 'logo.jpg'])
            ->call('POST', '/stands/66/event/8/book', $params);
        
        $this->json('GET', '/stands/event/8')
            ->seeJson([
                "status" => "SUCCESS",
                "message" => "Stands are found."
            ]);
    }
    
    /**
     * @covers \App\Modules\WebService\Controllers\StandController::companyDocument
     */
    public function testCompanyDocumentFormIdFailure()
    {
        $this->json('POST', '/company/document', ['field' => 'logo'])
            ->seeJson([
                "status" => "FAILURE",
                "message" => "Form id is invalid."
            ]);
    }
    
    /**
     * @covers \App\Modules\WebService\Controllers\StandController::companyDocument
     */
    public function testCompanyDocumentUploadPathFailure()
    {
        $uploads_path = public_path('uploads/document');
        $form_id = '123456';
        set_error_handler(function () { return true; });
        mkdir($uploads_path, 0755, true);
        restore_error_handler();
        touch($uploads_path . '/' . $form_id);
        
        $this->json('POST', '/company/document', ['form_id' => $form_id, 'stand_id' => 500, 'event_id' => 500])
            ->seeJson([
                'status' => 'FAILURE',
                'message' => 'Upload folder is not created.'
            ]);
        
        set_error_handler(function () { return true; });
        unlink($uploads_path . '/' . $form_id);
        restore_error_handler();
    }
    
    /**
     * @covers \App\Modules\WebService\Controllers\StandController::companyDocument
     */
    public function testCompanyDocumentUploadPathWritableFailure()
    {
        $uploads_path = public_path('uploads/document');
        $form_id = '1234567';
        set_error_handler(function () { return true; });
        mkdir($uploads_path . '/' . $form_id, 0555, true);
        restore_error_handler();
        
        $this->json('POST', '/company/document', ['form_id' => $form_id, 'stand_id' => 500, 'event_id' => 500])
            ->seeJson([
                'status' => 'FAILURE',
                'message' => 'Upload folder is not writable.'
            ]);
        
        set_error_handler(function () { return true; });
        chmod($uploads_path . '/' . $form_id, 0755);
        rmdir($uploads_path . '/' . $form_id);
        restore_error_handler();
    }
    
    /**
     * @covers \App\Modules\WebService\Controllers\StandController::companyDocument
     */
    public function testCompanyDocumentEmptyFileFailure()
    {
        $this->json('POST', '/company/document', 
                ['form_id' => '123456789', 'file' => '', 'stand_id' => 500, 'event_id' => 500])
            ->seeJson([
                'status' => 'FAILURE',
                'message' => 'File is invalid.'
            ]);
    }
    
    /**
     * @covers \App\Modules\WebService\Controllers\StandController::companyDocument
     */
    public function testCompanyDocumentFileFailure()
    {
        $filename = base_path('tests/mocks/logo.jpg');
        
        $name = str_random(8).'.png';
        $path = sys_get_temp_dir().'/'.$name;

        copy($filename, $path);
        
        $file = new \Illuminate\Http\UploadedFile($path, 'logo.jpg', 'image/jpeg', filesize($path), UPLOAD_ERR_CANT_WRITE, true);
        
        $this->json('POST', '/company/document', 
                ['form_id' => '123456789', 'file' => $file, 'stand_id' => 500, 'event_id' => 500])
            ->seeJson([
                'status' => 'FAILURE',
                'message' => 'The file "logo.jpg" could not be written on disk.'
            ]);
    }
    
    /**
     * @covers \App\Modules\WebService\Controllers\StandController::companyDocument
     */
    public function testCompanyDocumentFileSuccess()
    {
        $filename = base_path('tests/mocks/logo.jpg');
        
        $name = str_random(8).'.png';
        $path = sys_get_temp_dir().'/'.$name;

        copy($filename, $path);
        
        $file = new \Illuminate\Http\UploadedFile($path, 'logo.jpg', 'image/jpeg', filesize($path), null, true);
        
        $this->json('POST', '/company/document', 
                ['form_id' => '123456789', 'file' => $file, 'stand_id' => 500, 'event_id' => 500])
            ->seeJson([
                'status' => 'SUCCESS',
                'message' => 'File is uploaded.'
            ]);
    }
    
    /**
     * @covers \App\Modules\WebService\Controllers\StandController::book
     * @covers \App\Services\CommonService::validateUploads
     */
    public function testBookDocumentFailure()
    {
        $this->withSession(['file_error' => 'Error text'])
            ->json('POST', '/stands/66/event/8/book')
            ->seeJson([
                'status' => 'FAILURE',
                'message' => 'Company document or logo is not uploaded. Error text'
            ]);
    }
    
    /**
     * @covers \App\Modules\WebService\Controllers\StandController::book
     * @covers \App\Services\CommonService::validateUploads
     */
    public function testBookInvalidRequest()
    {
        $this->withSession(['document' => 'file1.pdf', 'logo' => 'logo.jpg'])
            ->json('POST', '/stands/12345/event/6789/book')
            ->seeJson([
                'status' => 'FAILURE',
                'message' => 'Invalid request'
            ]);
    }
    
    /**
     * @covers \App\Modules\WebService\Controllers\StandController::book
     * @covers \App\Services\CommonService::validateUploads
     * @covers \App\Services\CompanyService::addCompany
     */
    public function testBookCompanyFailure()
    {
        $this->withSession(['document' => 'file1.pdf', 'logo' => 'logo.jpg'])
            ->json('POST', '/stands/66/event/8/book')
            ->seeJson([
                'status' => 'FAILURE',
                'message' => 'Reserving of stand is failed.'
            ]);
    }
    
    /**
     * @covers \App\Modules\WebService\Controllers\StandController::book
     * @covers \App\Services\CommonService::validateUploads
     * @covers \App\Services\CommonService::cleanUploadSession
     * @covers \App\Services\CompanyService::addCompany
     * @covers \App\Services\UserService::addUser
     * @covers \App\Services\StandService::bookStand
     * @covers \App\Providers\StandServiceProvider::register
     * @covers \App\Providers\CommonServiceProvider::register
     * @covers \App\Providers\CompanyServiceProvider::register
     * @covers \App\Providers\UserServiceProvider::register
     */
    public function testBookSuccess()
    {
        $params = [
            'company' => 'Company',
            'firstname' => 'Rustamjon',
            'lastname' => 'Mukhammadaliyev',
            'email' => 'rustaonline@yahoo.com',
        ];
        
        $this->withSession(['document' => 'file1.pdf', 'logo' => 'logo.jpg'])
            ->json('POST', '/stands/66/event/8/book', $params)
            ->seeJson([
                'status' => 'SUCCESS',
                'message' => 'Stand is booked.'
            ]);
    }
    
    /**
     * @covers \App\Modules\WebService\Controllers\StandController::companyDocument
     */
    public function testStandAlreadyBooked()
    {
        $params = [
            'company' => 'Company',
            'firstname' => 'Rustamjon',
            'lastname' => 'Mukhammadaliyev',
            'email' => 'rustaonline@yahoo.com',
        ];
        
        $this->withSession(['document' => 'file1.pdf', 'logo' => 'logo.jpg'])
                ->call('POST', '/stands/67/event/8/book', $params);
        
        $this->json('POST', '/company/document', ['form_id' => uniqid(), 'file' => null, 'stand_id' => 67, 'event_id' => 8])
            ->seeJson([
                'status' => 'FAILURE',
                'message' => 'Stand has reserved already.'
            ]);
    }
}
