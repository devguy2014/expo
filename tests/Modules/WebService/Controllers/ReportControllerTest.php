<?php

use App\Models\Hall;
use App\Models\Stand;
use App\Models\Event;

/**
 * Description of ReportControllerTest
 *
 * @author Rustamjon Mukhammadaliyev <rustaonline@yahoo.com>
 */
class ReportControllerTest extends TestCase
{
    /**
     * Hall for test
     * @var \App\Models\Hall
     */
    private $hall;
    
    /**
     * Stand for test
     * @var \App\Models\Stand
     */
    private $stand;
    
    /**
     * Event for test
     * @var \App\Models\Event
     */
    private $event;
    
    /**
     * @covers \App\Modules\WebService\Controllers\ReportController::send
     * @covers \App\Services\CommonService::getFinishedEvents
     * @covers \App\Services\CommonService::eventMailed
     */
    public function testSend()
    {
        putenv('DAYS_DIFFERENCE=1000');
        
        $this->json('GET', '/send')
             ->seeJson([
                 'status' => 'SUCCESS',
                 'message' => 'No mail sent'
             ]);
    }
    
    /**
     * @covers \App\Modules\WebService\Controllers\ReportController::send
     * @covers \App\Services\CommonService::getFinishedEvents
     * @covers \App\Services\CommonService::eventMailed
     */
    public function testSendNotification()
    {
        
        putenv('DAYS_DIFFERENCE=1');
        
        $this->prepareData();
        
        $this->json('GET', '/send')
             ->seeJson([
                 'status' => 'SUCCESS',
                 'message' => 'Notifying is finished'
             ]);
        
        $this->cleanData();
    }
    
    private function prepareData()
    {
        
        $this->hall = new Hall(['lat' => 41.105, 'lng' => 69.248, 'image' => 'hall1.png',  'name' => 'My Test Hall']);
        $this->hall->save();
        
        $date = date('Y-m-d', time() - 84600);
        $this->event = new Event([
            'name' => 'My Test Event',
            'start_date' => $date,
            'end_date' => $date,
            'hall_id' => $this->hall->id]);
        $this->event->save();
        
        $this->stand = new Stand([
            'hall_id' => $this->hall->id,
            'name' => 'My Test Stand']);
        $this->stand->save();
        
        $params = [
            'company' => 'Company',
            'firstname' => 'Rustamjon',
            'lastname' => 'Mukhammadaliyev',
            'email' => 'rustaonline@yahoo.com',
        ];
        
        $this->withSession(['document' => 'file1.pdf', 'logo' => 'logo.jpg'])
            ->json('POST', '/stands/' . $this->stand->id . '/event/' . $this->event->id . '/book', $params);
            
    }
    
    protected function cleanData()
    {
        $this->hall->delete();
        $this->event->delete();
        $this->stand->delete();
    }
}
